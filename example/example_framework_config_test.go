package example

import (
	"gitee.com/bobo-rs/creative-services/framework/service"
	"testing"
)

func TestConfig_RemoveCache(t *testing.T) {
	service.Config().RemoveConfigCache(Ctx, `sms_driver`)
}
