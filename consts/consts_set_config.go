package consts

const (
	SafeSecret1HourRefreshLimitNumTagMac   = 300    // 同一个Mac刷新同一个Tag1小时内只允许300次
	SafeSecret1MinuteRefreshLimitNumTagMac = 10     // 同一个Mac刷新同一个Tag一分钟内只允许10次
	SafeSecret1MinuteRefreshLimitNumTag    = 600    // 同一个Tag每分钟允许刷新600次
	SafeSecret24HourRefreshLimitNumTag     = 100000 // 同一个Tag每天允许刷新10W次

)

/*
*

	3.1 同一手机号一分钟内只能发送一次：idea.space.safe.sms.captcha.mobile.1minute.send.lock:{mobile} // 同一手机号锁定一分钟
	3.3 同一个Mac地址一分钟内发送次数一次：idea.space.safe.sms.captcha.mac.1minute.send.lock:{mac} // 同一Mac锁定一分钟
	3.4 同一个Mac地址半时不能超过10次：idea.space.safe.sms.captcha.mac.half.hour.send.num:{mac} // 同一Mac半小时发送数量
	3.5 同一个手机号一小时不超过10次：idea.space.safe.sms.captcha.mobile.1hour.send.num // 同一手机号一小时发送数量
	3.2 在一分钟内不能超过120次发送：idea.space.safe.sms.captcha.1minute.send.num // 一分钟发送数量
	3.6 一小时内短信验证码发送量不超过600次：idea.space.safe.sms.captcha.24hour.send.num // 1小时发送数量
	3.7 24小时内不超过3000次：idea.space.safe.sms.captcha.24hour.send.num // 24小时发送数量
*/
const (
	SmsCaptchaHalfHourSendNumMac = 10   // 同一个Mac半小时发送数量不存超过10次
	SmsCaptcha1HourSendNumMobile = 10   // 同一个手机号一个小时发送数量不超过10次
	SmsCaptcha1MinuteSendTotal   = 120  // 一分钟内发送验证码数量不超过120次
	SmsCaptcha1HourSendTotal     = 600  // 一小时发送短信数量不超过600次
	SmsCaptcha24HourSendTotal    = 3000 // 24小时内发送短信数量不超过3000次
)

// 业务配置
const (
	SysConfigSmsDriver = `sms_driver` // 短信驱动名
)

// 登录安全配置
const (
	LoginMacLockNum        = 5 // 同一个登录MAC地址错误5次锁定20分钟
	LoginMacAccountLockNum = 5 // 同一个登录账户MAC地址错误5次锁定20分钟
)
