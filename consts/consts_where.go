package consts

const (
	OrmWhereLike    = ` Like ?`
	OrmWhereBetween = ` Between ? And ? `
	OrmWhereGT      = ` > ?`
	OrmWhereGTE     = ` >= ?`
	OrmWhereLT      = ` > ?`
	OrmWhereLTE     = ` > ?`
)
