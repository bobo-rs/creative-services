package consts

// HTTP 请求方式
const (
	HttpMethodGet    = `GET`
	HttpMethodPost   = `POST`
	HttpMethodHeader = `HEADER`
	HttpMethodCookie = `COOKIE`
	HttpMethodPut    = `PUT`
	HttpMethodDelete = `DELETE`
)
