package model

import "github.com/gogf/gf/v2/net/ghttp"

type (
	// UploadFileInput 上传文件请求参数
	UploadFileInput struct {
		UploadFileItem
	}

	// UploadFileOutput 上传文件输出结果
	UploadFileOutput struct {
		UploadAttachmentItem
	}

	// UploadFileItem 文件上传请求属性
	UploadFileItem struct {
		File       *ghttp.UploadFile `json:"file" dc:"上传对象"`
		Name       string            `json:"name" dc:"自定义文件名"`
		RandomName bool              `json:"random_name" dc:"是否随机命名文件"`
	}

	// UploadFileResponseItem 上传文件响应属性
	UploadFileResponseItem struct {
		Id          string `json:"id" dc:"文件ID"`
		Name        string `json:"name" dc:"文件名"`
		Path        string `json:"path" dc:"文件路径"`
		Url         string `json:"url" dc:"访问Url"`
		Width       uint   `json:"width" dc:"图片宽度"`
		Height      uint   `json:"height" dc:"图片高度"`
		Size        uint   `json:"size" dc:"文件大小"`
		StorageType string `json:"storage_type" dc:"存储类型"`
	}
)
