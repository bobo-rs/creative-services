package model

type (
	// SmsRecordListInput 获取短信记录列表请求参数
	SmsRecordListInput struct {
		CommonPaginationItem
		SmsRecordWhereItem
	}

	// SmsRecordListOutput 获取短信记录列表响应结果
	SmsRecordListOutput struct {
		CommonResponseItem
		Rows []*SmsRecordDetailItem `json:"rows" dc:"数据集"`
	}
)

type (
	// SmsRecordWhereItem 短信记录搜索条件属性
	SmsRecordWhereItem struct {
		Mobile  string `json:"mobile" dc:"手机号"`
		Content string `json:"content" dc:"短信内容"`
		SmsType int    `json:"sms_type" dc:"短信类型：0验证码，1消息通知，2营销短信"`
	}

	// SmsSendRecordNewItem 通过手机号获取短信发送最新记录
	SmsSendRecordNewItem struct {
		SmsRecordItem
	}

	// SmsRecordDetailItem 短信记录详情属性
	SmsRecordDetailItem struct {
		SmsRecordItem
		SmsRecordExtItem
		FmtSmsType string `json:"fmt_sms_type" dc:"格式化短信类型"`
	}

	// SmsRecordItem 短信发送记录属性
	SmsRecordItem struct {
		Id      uint   `json:"id" dc:"短信发送ID"`
		Mobile  string `json:"mobile" dc:"手机号"`
		Content string `json:"content" dc:"短信内容"`
		Code    string `json:"code" dc:"验证码"`
		SmsType uint   `json:"sms_type" dc:"短信类型：0验证码，1消息通知，2营销短信"`
		Results string `json:"results" dc:"发送结果"`
	}

	// SmsRecordExtItem 短信记录扩展属性
	SmsRecordExtItem struct {
		Ip         string `json:"ip" dc:"IP"`
		TemplateId string `json:"template_id" dc:"平台模板ID"`
		CreateAt   string `json:"create_at" dc:"创建时间"`
	}
)
