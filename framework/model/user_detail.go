package model

type (
	// UserUpdatePwdInput 用户-修改密码请求参数
	UserUpdatePwdInput struct {
		UserUpdatePwdItem
	}

	// UserUpdateDetailInput 用户修改用户详情请求参数
	UserUpdateDetailInput struct {
		UserUpdateDetailItem
	}

	// UserForgotPwdSendCaptchaInput 找回密码-发送短信验证码
	UserForgotPwdSendCaptchaInput struct {
		Mobile string `json:"mobile" dc:"手机号-加密后"`
		UserOauthLoginSignItem
	}

	// UserForgotPwdValidateCaptchaInput 找回密码-校验发送短信验证码
	UserForgotPwdValidateCaptchaInput struct {
		Code string `json:"code" dc:"短信验证码"`
	}

	// UserForgotPwdUpdatePwdInput 找回密码-修改密码
	UserForgotPwdUpdatePwdInput struct {
		Pwd string `json:"pwd" dc:"密码"`
	}
)

type (
	// UserAccountLoginPwdItem 用户账户登录密码属性
	UserAccountLoginPwdItem struct {
		PwdHash string `json:"pwd_hash" dc:"密码Hash值"`
		PwdSalt string `json:"pwd_salt" dc:"密码盐"`
	}

	// UserBasicDetailItem 用户基础详情属性
	UserBasicDetailItem struct {
		Nickname string `json:"nickname" dc:"昵称"`
		UserId   uint   `json:"user_id" dc:"用户ID"`
		Sex      uint   `json:"sex" dc:"性别"`
		Birthday string `json:"birthday" dc:"生日"`
		Avatar   string `json:"avatar" dc:"头像"`
		AttachId string `json:"attach_id" dc:"附件ID"`
		SignNum  uint   `json:"sign_num" dc:"签到总数"`
		ConNum   uint   `json:"con_num" dc:"联系签到次数"`
	}

	// UserUpdatePwdItem 用户修改密码
	UserUpdatePwdItem struct {
		Pwd  string `json:"pwd" dc:"密码"`
		Code string `json:"code" dc:"验证码"`
	}

	// UserUpdateDetailItem 用户详情修改
	UserUpdateDetailItem struct {
		Sex      uint   `json:"sex" dc:"性别"`
		Birthday string `json:"birthday" dc:"生日"`
		AttachId string `json:"attach_id" dc:"附件ID"`
		Nickname string `json:"nickname" dc:"昵称"`
	}
)
