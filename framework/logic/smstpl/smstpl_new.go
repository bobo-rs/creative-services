package smstpl

import "gitee.com/bobo-rs/creative-services/framework/service"

type sSmsTpl struct {
}

func init() {
	service.RegisterSmsTpl(New())
}

func New() *sSmsTpl {
	return &sSmsTpl{}
}
