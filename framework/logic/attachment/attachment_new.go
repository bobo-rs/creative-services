package attachment

import "gitee.com/bobo-rs/creative-services/framework/service"

type sAttachment struct {
}

func init() {
	service.RegisterAttachment(New())
}

func New() *sAttachment {
	return &sAttachment{}
}
