package user

import "gitee.com/bobo-rs/creative-services/framework/service"

type sUser struct {
}

func init() {
	service.RegisterUser(New())
}

func New() *sUser {
	return &sUser{}
}
