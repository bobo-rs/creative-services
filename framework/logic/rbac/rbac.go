package rbac

import "gitee.com/bobo-rs/creative-services/framework/service"

func init() {
	service.RegisterRbac(New())
}
