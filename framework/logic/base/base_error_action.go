package base

import (
	"errors"
	"github.com/gogf/gf/v2/frame/g"
)

// WhereErr 条件查询错误
func (s *TblBaseService) WhereErr(where interface{}) error {
	if where == nil || g.IsNil(where) {
		return errors.New(`缺少请求参数`)
	}
	return nil
}
