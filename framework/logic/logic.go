package logic

import (
	_ "gitee.com/bobo-rs/creative-services/framework/logic/article"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/attachment"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/bizctx"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/common"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/config"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/middleware"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/rbac"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/smstpl"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/syslog"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/sysmap"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/upload"
	_ "gitee.com/bobo-rs/creative-services/framework/logic/user"
)
