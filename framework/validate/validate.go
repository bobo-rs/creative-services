package validate

// RuleRegister 自定义注册规则
func RuleRegister() {
	// 注册附件
	RuleRegisterAttachment()
	// 注册公共模块
	RuleRegisterCommon()
	// 注册配置
	RuleRegisterConfig()
	// 注册短信模板
	RuleRegisterSmsTemplate()
	// 注册RBAC规则
	RuleRegisterRbac()
	// 注册用户规则
	RuleRegisterUser()
	// 注册文章规则
	RuleRegisterArticle()
	// 注册系统字典规则
	RuleRegisterSysMap()
}
