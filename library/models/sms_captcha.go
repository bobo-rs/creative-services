package models

type (
	// SmsSendItem 验证码发送规则
	SmsSendItem struct {
		Mobile         []string `json:"mobile" dc:"手机号"`
		TemplateId     string   `json:"template_id" dc:"模板ID"`
		TemplateParams []string `json:"template_params" dc:"模板参数"`
	}
)
