package models

type (
	// UserForgotPwdParseItem 用户找回密码解析参数
	UserForgotPwdParseItem struct {
		AuthSignItem
		Mobile   string // 加密手机号
		IsRemove bool
	}

	// ForgotPwdValidateItem 找回密码-校验规则
	ForgotPwdValidateItem struct {
		Step     byte // 验证步骤：0校验验证码，1修改密码
		IsRemove bool // 是否销毁：false否，true销毁
	}

	// ForgotPwdValidateResultItem 找回密码-验证码校验结果
	ForgotPwdValidateResultItem struct {
		Mobile string // 手机号
	}
)
