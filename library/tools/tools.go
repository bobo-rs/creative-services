package tools

import (
	"fmt"
	"gitee.com/bobo-rs/creative-framework/pkg/utils"
	"gitee.com/bobo-rs/creative-services/consts"
	"github.com/gogf/gf/v2/util/grand"
)

// GenAccount 生成账户名
func GenAccount(ps ...string) string {
	prefix := consts.AccountPrefix
	if len(ps) > 0 {
		prefix = ps[0]
	}
	return fmt.Sprintf(`%s%s-%d`, prefix, grand.S(5), utils.RandInt(4))
}
