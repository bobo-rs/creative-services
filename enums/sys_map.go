package enums

type SysMapDisable uint

const (
	SysMapDisableOk SysMapDisable = iota
	SysMapDisableDisable
)

func (s SysMapDisable) Fmt() string {
	switch s {
	case SysMapDisableOk:
		return `正常`
	case SysMapDisableDisable:
		return `禁用`
	default:
		return ``
	}
}
