package enums

type SmsDriver string

const (
	SmsDriverTencent SmsDriver = `sms_tencent` // 腾讯服务
	SmsDriverAlipay  SmsDriver = `sms_alipay`  // 阿里云
)

func (d SmsDriver) Fmt() string {
	switch d {
	case SmsDriverTencent:
		return `腾讯云`
	case SmsDriverAlipay:
		return `阿里云`
	default:
		return ``
	}
}
