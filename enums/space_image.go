package enums

type (
	// SpaceImagePrecisionEnums 图片空间精度枚举类型
	SpaceImagePrecisionEnums  uint
	SpaceImageDesignTypeEnums uint
	SpaceImageSortTypeEnums   uint // 设计排序
)

const (
	SpaceImagePrecisionZero SpaceImagePrecisionEnums = iota
	SpaceImagePrecisionOne
	SpaceImagePrecisionTwo
	SpaceImagePrecisionThree
	SpaceImagePrecisionFour
	SpaceImagePrecisionFive
	SpaceImagePrecisionSix
	SpaceImagePrecisionSeven
	SpaceImagePrecisionEight
	SpaceImagePrecisionNine
	SpaceImagePrecisionTen
)

const (
	SpaceImageDesignTypeSmart SpaceImageDesignTypeEnums = iota
	SpaceImageDesignTypeHandMove
	SpaceImageDesignTypeActive
)

const (
	SpaceImageSortTypeDefault SpaceImageSortTypeEnums = iota
	SpaceImageSortTypeNew
	SpaceImageSortTypeAtMost
)

// Fmt 格式化图片空间精度
func (e SpaceImagePrecisionEnums) Fmt() string {
	switch e {
	case SpaceImagePrecisionZero, SpaceImagePrecisionOne, SpaceImagePrecisionTwo:
		return `低精度`
	case SpaceImagePrecisionThree, SpaceImagePrecisionFour, SpaceImagePrecisionFive:
		return `中精度`
	case SpaceImagePrecisionTen:
		return `超高精度`
	default:
		return `高精度`
	}
}

func (e SpaceImageSortTypeEnums) Fmt() string {
	switch e {
	case SpaceImageSortTypeNew:
		return `id desc` // 最新发布
	case SpaceImageSortTypeAtMost:
		return `download_num desc, id desc` // 下载量 > 最新
	default:
		return `download_num desc, browse desc, id desc` // 默认：下载量 > 浏览量>最新
	}
}
