package enums

type (
	LogOperateType int
)

const (
	LogOperateTypeOperate LogOperateType = iota
	LogOperateTypeLogin
	LogOperateTypeSystem
)

func (t LogOperateType) Fmt() string {
	switch t {
	case LogOperateTypeOperate:
		return `操作日志`
	case LogOperateTypeLogin:
		return `登录日志`
	case LogOperateTypeSystem:
		return `系统日志`
	default:
		return ``
	}
}
