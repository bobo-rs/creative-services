package enums

type TencentSmsInternational uint64
type TencentSmsStatusCode int

const (
	TencentSmsInternationalChina TencentSmsInternational = 0
	TencentSmsInternationalHMTC  TencentSmsInternational = 1
	TencentSmsInternationalAll   TencentSmsInternational = 3
	TencentSmsStatusCodeAF       TencentSmsStatusCode    = -1
	TencentSmsStatusCodeAE       TencentSmsStatusCode    = 0
	TencentSmsStatusCodeAR       TencentSmsStatusCode    = 1
	TencentSmsStatusCodeTE       TencentSmsStatusCode    = 2
)

// Fmt 腾讯短信类型
func (i TencentSmsInternational) Fmt() string {
	switch i {
	case TencentSmsInternationalChina:
		return `国内短信`
	case TencentSmsInternationalHMTC:
		return `国际/港澳台短信`
	case TencentSmsInternationalAll:
		return `国内短信也支持国际/港澳台短信`
	default:
		return ``
	}
}

// Fmt 腾讯短信模板状态
func (c TencentSmsStatusCode) Fmt() string {
	switch c {
	case TencentSmsStatusCodeAF:
		return `审核未通过`
	case TencentSmsStatusCodeAE:
		return `审核通过已生效`
	case TencentSmsStatusCodeAR:
		return `审核中`
	case TencentSmsStatusCodeTE:
		return `审核通过生效中`
	default:
		return ``
	}
}
